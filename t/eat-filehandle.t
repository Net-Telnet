#!/usr/bin/env perl

use Test::More;
use Test::Exception;
use Net::Telnet;
use IO::Handle;


open(my $fh, "<", "/dev/zero") or die $!;

lives_ok { $fh->untaint };

done_testing();
